# **Macro Documentation Helpers** plugin for Confluence

The **Macro Documentation Helpers** plugin contains two macros and one blueprint:

* [Macro Information](https://bitbucket.org/seuqra/confluence-macrodoc/wiki/Macro%20Information%20User%20Guide)
* [Macro Tutorial](https://bitbucket.org/seuqra/confluence-macrodoc/wiki/Macro%20Tutorial%20User%20Guide)
* [Macro Information Blueprint](https://bitbucket.org/seuqra/confluence-macrodoc/wiki/Macro%20Information%20Blueprint%20User%20Guide)