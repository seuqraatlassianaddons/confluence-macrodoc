package com.seuqra.confluence.macrodoc;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.macro.browser.MacroMetadataManager;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.message.I18nResolver;

	/**
	 * A resource of message.
	 */
	@Path("/macros")
	public class MacrosMetaDataResource {
		
		private MacroMetadataManager macroMetadataManager;
		private I18nResolver i18n;

		public MacrosMetaDataResource(MacroMetadataManager macroMetadataManager, I18nResolver i18n) {
			this.macroMetadataManager = macroMetadataManager;
			this.i18n = i18n;
		}

	    @GET
	    @AnonymousAllowed
	    @Produces({MediaType.APPLICATION_JSON})

	    public Response getMessage() {
	    	Set<MacroMetadata> macrosMetadata = macroMetadataManager.getAllMacroMetadata();
	    	Set<MacroMetaDataResourceModel> macrosMetaDataResourceModel = new TreeSet<MacroMetaDataResourceModel>();
	    	Iterator<MacroMetadata> iterator = macrosMetadata.iterator();
	    	while (iterator.hasNext()) {
	    		macrosMetaDataResourceModel.add(new MacroMetaDataResourceModel(iterator.next(),i18n));
	    	}
            return Response.ok(macrosMetaDataResourceModel).build();
	    }
	}
