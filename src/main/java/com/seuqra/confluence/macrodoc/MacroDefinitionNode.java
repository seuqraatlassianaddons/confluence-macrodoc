package com.seuqra.confluence.macrodoc;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

public class MacroDefinitionNode {
	
	private MacroDefinition macroDefinition;
	private List<MacroDefinitionNode> children;
	private MacroMetadata macroMetadata;
	
	public MacroDefinitionNode (MacroDefinition macroDefinition, MacroMetadata macroMetadata, List<MacroDefinitionNode> children) {
		this.macroDefinition = macroDefinition;
		this.macroMetadata = macroMetadata;
		this.children = children;
	}
	
	public MacroDefinitionNode (MacroDefinition macroDefinition, MacroMetadata macroMetaData) {
		this(macroDefinition, macroMetaData, new ArrayList<MacroDefinitionNode>());
	}
	
	public MacroDefinition getMacroDefinition() {
		return macroDefinition;
	}
	
	public MacroMetadata getMacroMetadata() {
		return macroMetadata;
	}
	
	@HtmlSafe
	public String getMacroBodyText() {
		return macroDefinition.getBodyText();
	}
	
	@HtmlSafe
	public String getMacroBody() {
		return macroDefinition.getBody().getBody();
	}
	
	public List<MacroDefinitionNode> getChildren() {
		return children;
	}
	
	MacroDefinitionNode addChild(MacroDefinitionNode macroDefinitionNode) {
		children.add(macroDefinitionNode);
		return macroDefinitionNode;
	}
	
	public String toString() {
		StringBuilder tree = new StringBuilder();
		if (macroDefinition != null) {
			//tree.append("\n");
			tree.append(macroDefinition.getName());
			tree.append("-->");
		}
		for (MacroDefinitionNode macroDefinitionNode : getChildren()) {
			//tree.append(" ");
			tree.append(" "+ macroDefinitionNode.toString());
			tree.append("\n");

		}
		return tree.toString();
		
	}
}
