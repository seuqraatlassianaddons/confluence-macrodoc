package com.seuqra.confluence.macrodoc;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.browser.MacroMetadataManager;
import com.atlassian.confluence.macro.browser.beans.MacroFormDetails;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.macro.browser.beans.MacroParameter;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.MacroDefinitionReplacer;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.confluence.xhtml.api.XhtmlVisitor;
import com.atlassian.sal.api.message.I18nResolver;

public class Utils {
	private static Log log = LogFactory.getLog("com.seuqra.confluence.macrodoc");
	
	public static String translate(String key, I18nResolver i18n) {
		return translate(key, i18n, (Serializable[])null);
	}
	
	/*
	 * Return the translation corresponding to the key.
	 * @param key the key to translate
	 * @param i18n the internationalization resolver
	 * @return the translation corresponding to the key if it exists, empty String otherwise (instead of the key)
	 */
	public static String translate(String key, I18nResolver i18n, Serializable... arguments) {
		if (key == null) {
			return "";
		}
		String translation = null;
		if (arguments != null) {
			translation = i18n.getText(key, arguments);
		} else {
			translation = i18n.getText(key);
		}
		if (translation == null || (translation.contains(".") && translation.equals(key))) {
			//TODO : When a key has no translation and contains the pattern param.xxx.label, return xxx 
			translation = "";
		}
		return translation;
	}
	
	/*
	 * Return a List of MacroDefinition contained in the xml body sorted by reverse order of appearance in the xml body
	 * @return a List of MacroDefinition contained in the xml body
	 */
	public static List<MacroDefinition> parseMacroDefinition0(final String xmlBody, final ConversionContext conversionContext, XhtmlContent xhtmlContent)  {
		final List<MacroDefinition> macroDefinitions = new ArrayList<MacroDefinition>();
		 
		try {
			xhtmlContent.handleMacroDefinitions(xmlBody, conversionContext, new MacroDefinitionHandler() {
				public void handle(final MacroDefinition macroDefinition) {
					macroDefinitions.add(macroDefinition);
					log.debug("Macro Name = "+ macroDefinition.getName());
					log.debug("Macro Body  = "+ macroDefinition.getBody());
					log.debug("Macro Body text = "+ macroDefinition.getBodyText());
		        }	
			}/*,  MacroDefinitionMarshallingStrategy.MARSHALL_MACRO*/);
		} catch (XhtmlException e) {
		    log.error(e);
		}
		return macroDefinitions;
	}
		
	public static String getMacroId(MacroDefinition macroDefinition) {
		String macroId = null;
		// Keep compatibility with version less than 5.8
		//Replace macroDefinition.getMacroId().get().getId() by following block

		try {
			Method method;
			method = MacroDefinition.class.getMethod("getMacroId");
			// object = Option<MacroId>
			Object optionMacroIdObj = method.invoke(macroDefinition);
			method = optionMacroIdObj.getClass().getMethod("get");
			method.setAccessible(true);
			// object = MacroId
			Object macroIdObj = method.invoke(optionMacroIdObj);
			method = macroIdObj.getClass().getMethod("getId");
			macroId = (String) method.invoke(macroIdObj);
		} catch (Exception e) {
			log.debug(" No getMacroId method on MacroDefinition -> Confluence version in less than 5.8");
		}
		return macroId;
	}
	
	/*
	 * Return a List of MacroDefinition contained in the xml body sorted by reverse order of appearance in the xml body
	 * @return a List of MacroDefinition contained in the xml body
	 */
	private static Map<String, MacroDefinition> computeMacroDefinitionMap(final String xmlBody, final ConversionContext conversionContext, XhtmlContent xhtmlContent)  {
		final Map<String, MacroDefinition> macroDefinitions = new TreeMap<String, MacroDefinition>();
		 
		try {
			xhtmlContent.handleMacroDefinitions(xmlBody, conversionContext, new MacroDefinitionHandler() {
				public void handle(final MacroDefinition macroDefinition) {
					String macroId = getMacroId(macroDefinition);
					if (macroId != null) {
						// Since Confluence 5.8: macro-id has been added to uniquely identify a macro in a page
						log.debug("Found macro-id = "+ macroId);
						macroDefinitions.put(macroId, macroDefinition);
					} else if (!macroDefinitions.containsKey(macroDefinition.getName())) {
						// Confluence < 5.8
						// If the same macro is present twice in the body, keep only the first occurrence (not satisfying but don't know what to do ??)
						log.debug("No macro-id. Put macro name as key  = "+ macroDefinition.getName());
						macroDefinitions.put(macroDefinition.getName(), macroDefinition);
					}
					log.debug("Macro Name = "+ macroDefinition.getName());
					log.debug("Macro Body  = "+ macroDefinition.getBody());
					log.debug("Macro Body text = "+ macroDefinition.getBodyText());
		        }	
			}/*,  MacroDefinitionMarshallingStrategy.MARSHALL_MACRO*/);
		} catch (XhtmlException e) {
		    log.error(e);
		}
		return macroDefinitions;
	}
		
	/*
	 * Return a List of MacroDefinition contained in the xml body sorted by order of appearance in the xml body
	 * @return a List of MacroDefinition contained in the xml body
	 */
	public static List<MacroDefinition> computeMacroDefinitionList(final String xmlBody, final ConversionContext conversionContext, final XhtmlContent xhtmlContent)  {
		return new ArrayList<MacroDefinition> (computeMacroDefinitionMap(xmlBody, conversionContext, xhtmlContent).values());
	}
	
	/*
	 * Return a Map of MacroDefinition contained in the xml body. The key is the macro-id
	 * @return a Map of MacroDefinition with macro-id as key
	 */
	public static Map<String, MacroDefinition> computeMacroDefinitionMap1(final String xmlBody, final ConversionContext conversionContext, final XhtmlContent xhtmlContent)  {
		final Map<String, MacroDefinition> macroDefinitions = new TreeMap<String, MacroDefinition>();
		log.debug("xmlBody = "+ xmlBody);
		
		try {
		    xhtmlContent.replaceMacroDefinitionsWithString(xmlBody, conversionContext, new MacroDefinitionReplacer() {
		        public String replace(MacroDefinition macroDefinition) throws XhtmlException {
					String macroId = getMacroId(macroDefinition);
					if (macroId != null) {
						// Since Confluence 5.8: macro-id has been added to uniquely identify a macro in a page
						log.debug("Found macro-id = "+ macroId);
						macroDefinitions.put(macroId, macroDefinition);
					} else if (!macroDefinitions.containsKey(macroDefinition.getName())) {
						// Confluence < 5.8
						// If the same macro is present twice in the body, keep only the first occurrence (not satisfying but don't know what to do ??)
						log.debug("No macro-id. Put macro name as key  = "+ macroDefinition.getName());
						macroDefinitions.put(macroDefinition.getName(), macroDefinition);
					}
					log.debug("Macro Name = "+ macroDefinition.getName());
					log.debug("Macro Body  = "+ macroDefinition.getBody());
					log.debug("Macro Body text = "+ macroDefinition.getBodyText());
		            return xhtmlContent.convertMacroDefinitionToView(macroDefinition, conversionContext);
		        }
		    });
		} catch (XhtmlException e) {
		    log.error(e);
		}
		return macroDefinitions;
	}
	
	/*
	 * Return a MacroDefinitionNode containing the macro(s) defined in the body in a hierarchical representation
	 * @return a MacroDefinitionNode containing the macro(s) defined in the body in a hierarchical representation
	 */
	public static MacroDefinitionNode getMacroDefinitionNode(final String xmlBody,
			final ConversionContext conversionContext, XhtmlContent xhtmlContent, MacroMetadataManager macroMetadataManager) {
		List<XhtmlVisitor> visitors = new ArrayList<XhtmlVisitor>();
		Map <String, MacroDefinition> macroDefinitions = Utils.computeMacroDefinitionMap(xmlBody, conversionContext, xhtmlContent);

		MacroXhtmlVisitor2 macroXhtmlVisitor2 = new MacroXhtmlVisitor2(macroDefinitions, macroMetadataManager);
		visitors.add(macroXhtmlVisitor2);

		try {
			xhtmlContent.handleXhtmlElements(xmlBody, conversionContext,
					visitors);
		} catch (XhtmlException e) {
			log.error("Error during the parsing of the macro body",e);
		}
		return macroXhtmlVisitor2.getMacroDefinitionNode();
	}
		
	private static Map<String, String> translateParameters(String macroName, MacroMetadataManager macroMetadataManager, I18nResolver i18n) {
		Map<String, String> parameters = new LinkedHashMap<String, String>();
		MacroMetadata macroMetadata = macroMetadataManager.getMacroMetadataByName(macroName);
		MacroFormDetails macroFormDetails = macroMetadata.getFormDetails();
		List <MacroParameter> macroParameters = macroFormDetails.getParameters();
	
		for (MacroParameter macroParameter : macroParameters) {
				parameters.put(macroParameter.getName(), Utils.translate(macroParameter.getDisplayName().getKey(), i18n));
		}			
		return parameters;		
	}
	
	private static class MacroXhtmlVisitor2 implements XhtmlVisitor {
		MacroDefinitionNode rootNode = null;
		Stack<MacroDefinitionNode> stack = null;
		Map <String, MacroDefinition> macroDefinitions;
		private MacroMetadataManager macroMetadataManager;
		
		MacroXhtmlVisitor2(Map <String, MacroDefinition> macroDefinitions, MacroMetadataManager macroMetadataManager) {
			this.macroDefinitions = macroDefinitions;
			this.macroMetadataManager = macroMetadataManager;
		}
		
		public MacroDefinitionNode getMacroDefinitionNode() {
			return rootNode;
		}
	
		@Override
		public boolean handle(XMLEvent xmlEvent, ConversionContext context) {
			if (xmlEvent.isStartElement()) {
				StartElement startElement = xmlEvent.asStartElement();
				String elementName = startElement.getName().getLocalPart();
				if ("structured-macro".equals(elementName)) {
					Iterator i = startElement.getAttributes();
					String macroName = null;
					String macroId = null;
					MacroDefinition macroDefinition = null;
					while (i.hasNext()) {
						Attribute a = (Attribute) i.next();
						if (("macro-id".equals(a.getName().getLocalPart()))) {								
							macroId = a.getValue();
						} else	if ("name".equals(a.getName().getLocalPart())) {
							macroName = a.getValue();

						}
					}
					if (macroId != null) {
						macroDefinition = macroDefinitions.get(macroId);	
					} else if (macroName != null) {
						macroDefinition = macroDefinitions.get(macroName);	
					}
					log.debug("macro-id "+macroId+ " found for macro "+macroName);
					if (macroDefinition != null) {
						MacroDefinitionNode currrentElement = null;
						if (stack == null) {
							stack = new Stack<MacroDefinitionNode>();
							rootNode = new MacroDefinitionNode(macroDefinition, macroMetadataManager.getMacroMetadataByName(macroName));
							currrentElement = rootNode;
						} else {
							currrentElement = stack.peek().addChild(new MacroDefinitionNode(macroDefinition, macroMetadataManager.getMacroMetadataByName(macroName)));
						}
						stack.push(currrentElement);
					} else {
						log.debug("No MacroDefinition found for macro "+macroName + " and macro-id "+macroId);
					}
				}

			} else if (xmlEvent.isEndElement()) {
				EndElement endElement = xmlEvent.asEndElement();
				String elementName = endElement.getName().getLocalPart();
				if ("structured-macro".equals(elementName)) {
					try {
						stack.pop();
					} catch (Exception e) {
						// In case a MacroDefinition was not pushed in the stack
					}
				}
			}
			return true;
		}
	}
}
