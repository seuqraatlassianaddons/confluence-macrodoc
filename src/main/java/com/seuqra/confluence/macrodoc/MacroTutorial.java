package com.seuqra.confluence.macrodoc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.browser.MacroMetadataManager;
import com.atlassian.confluence.macro.browser.beans.MacroFormDetails;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.macro.browser.beans.MacroParameter;
import com.atlassian.confluence.macro.browser.beans.MacroParameterType;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;

/**
 **/
public class MacroTutorial implements Macro {
	private static final String TOREPLACEBYXHTMLMACRO = "TOREPLACEBYXHTMLMACRO";
	private static final String MACRO_TUTORIAL = "macro-tutorial";
	private static final String STRING_PARAMETERS_IN_MACRO_BROWSER = "com.seuqra.confluence.macrodoc.macro-tutorial.parametersInMacroBrowser";
	private static final String STRING_DATA_IN_MACRO_PLACEHOLDER = "com.seuqra.confluence.macrodoc.macro-tutorial.dataInMacroPlaceholder";
	private static final String STRING_RENDERED_DATAS = "com.seuqra.confluence.macrodoc.macro-tutorial.renderedDatas";
	private static final String STRING_KEY = "com.seuqra.confluence.macrodoc.macro-tutorial.key";
	private static final String STRING_VALUE = "com.seuqra.confluence.macrodoc.macro-tutorial.value";
	private static final String STRING_NO_PARAMETER = "com.seuqra.confluence.macrodoc.macro-tutorial.noParameter";
	
	private static final String PARAM_TITLE = "title";

	private static final String ERROR = "com.seuqra.confluence.macrodoc.macro-tutorial.error";
	private static final String MACRO_DEFINITIONS = "macro_definitions";
	private static final String MACRO_PARAMETERS = "macro_parameters";
	private static final String MACRO_BODY = "macro_body";

	private static Log log = LogFactory.getLog("com.seuqra.confluence.macrodoc");
	private XhtmlContent xhtmlContent;
	private MacroMetadataManager macroMetadataManager;
	private I18nResolver i18n;
		
    public MacroTutorial(XhtmlContent xhtmlContent, MacroMetadataManager macroMetadataManager, I18nResolver i18n){
    	this.xhtmlContent = xhtmlContent;
    	this.macroMetadataManager = macroMetadataManager;
    	this.i18n = i18n;
    }
	
	@Override
	@RequiresFormat(Format.Storage)
	public String execute(Map<String, String> parameters, String xmlBody, ConversionContext conversionContext) throws MacroExecutionException {
    	Map<String, Object> context = MacroUtils.defaultVelocityContext();
		
		if (parameters.get(PARAM_TITLE) != null) {
			context.put(PARAM_TITLE, parameters.get(PARAM_TITLE));
			//result.append("<h1>").append(parameters.get(PARAM_TITLE)).append("</h1>");
		}

		MacroDefinitionNode macroDefinitionNode = Utils.getMacroDefinitionNode(xmlBody, conversionContext, xhtmlContent, macroMetadataManager);
		context.put(MACRO_DEFINITIONS, macroDefinitionNode);

		List<List<String>> macrosParameters = createParametersTable(macroDefinitionNode.getMacroDefinition(), Utils.translate(STRING_KEY, i18n), Utils.translate(STRING_VALUE, i18n), i18n);
		context.put(MACRO_PARAMETERS, macrosParameters);
		context.put(MACRO_BODY, xmlBody);
		context.put("title1", Utils.translate(STRING_PARAMETERS_IN_MACRO_BROWSER, i18n));
		context.put("title2", Utils.translate(STRING_DATA_IN_MACRO_PLACEHOLDER, i18n));
		context.put("title3", Utils.translate(STRING_RENDERED_DATAS, i18n));

		String result = VelocityUtils.getRenderedTemplate("templates/macro-tutorial.vm", context);
		// the body contains XHTML that is escaped by Velocity. I don't know how to avoid it
		// So after Velocity has generated the HTML code, I put the xhtmlBody String into the HTML body
		// The XHTML Body will be converted to HTML by the method xhtmlContent.convertStorageToView
		result = result.replace(TOREPLACEBYXHTMLMACRO, xmlBody);
		try {
			
			return xhtmlContent.convertStorageToView(result, conversionContext);
		} catch (Exception e) {
			log.error(e);
		} 
		return Utils.translate(ERROR, i18n);
	}
	
	private List<List<String>> createParametersTable(MacroDefinition macroDefinition, String titleColumn1, String titleColumn2, I18nResolver i18n) {
		Map<String, String> macroDefinitionParams =  macroDefinition.getParameters();
		List<List<String>> macrosParametersTable = new ArrayList<List<String>>();
		MacroMetadata macroMetadata = macroMetadataManager.getMacroMetadataByName(macroDefinition.getName());
		MacroFormDetails macroFormDetails = macroMetadata.getFormDetails();
		List <MacroParameter> macroParameters = macroFormDetails.getParameters();
		
		// First row contains the titles of the table
		addMacroParameters(macrosParametersTable, titleColumn1, titleColumn2);

		for (MacroParameter macroParameter : macroParameters) {
			String key = macroParameter.getName();
			if (macroDefinitionParams.containsKey(key) || macroParameter.getDefaultValue() != null) {
				String translatedParameterKey = Utils.translate(macroParameter.getDisplayName().getKey(), i18n);
				if (macroParameter.isRequired()) {
					translatedParameterKey = translatedParameterKey + " (*)";
				}
				String parameterValue = macroDefinitionParams.get(key);
				if (parameterValue == null) {
					parameterValue = macroParameter.getDefaultValue();
					// The value is not in the macroDefinitionParams List. Check if a default value is defined
					MacroParameterType macroParameterType = macroParameter.getType();
					 if (macroParameterType.equals(MacroParameterType.BOOLEAN) && !Boolean.parseBoolean(parameterValue)) {
						 parameterValue = null;
					 }					 
				}
				if (parameterValue != null) {
					addMacroParameters(macrosParametersTable, translatedParameterKey, parameterValue);
				}
			}			
		}
		if (macrosParametersTable.size() == 1) {
			// No parameter found or set
			macrosParametersTable.clear();
			List<String> emptyMacroParameters = new ArrayList<String>();
			emptyMacroParameters.add(Utils.translate(STRING_NO_PARAMETER, i18n));
			macrosParametersTable.add(emptyMacroParameters);
		}
		return macrosParametersTable;
	}

	private static void addMacroParameters(
			List<List<String>> macrosParametersTable,
			String translatedParameterKey, String parameterValue) {
		List<String> macroParametersRow = new ArrayList<String>();
		macroParametersRow.add(translatedParameterKey);
		macroParametersRow.add(parameterValue);
		macrosParametersTable.add(macroParametersRow);
	}
	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}
	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
