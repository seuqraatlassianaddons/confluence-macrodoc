package com.seuqra.confluence.macrodoc;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.confluence.macro.browser.MacroMetadataManager;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.AbstractBlueprintContextProvider;
import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.sal.api.message.I18nResolver;

public class MacroInformationBlueprintContextProvider extends AbstractBlueprintContextProvider  {

	private MacroMetadataManager macroMetadataManager;
	private I18nResolver i18n;
	private PageManager pageManager;
	private static Log log = LogFactory.getLog("com.seuqra.confluence.macrodoc");

	public MacroInformationBlueprintContextProvider(MacroMetadataManager macroMetadataManager, I18nResolver i18n, PageManager pageManager){
    	this.macroMetadataManager = macroMetadataManager;
    	this.i18n = i18n;
    	this.pageManager = pageManager;
	}
	
	@Override
	protected BlueprintContext updateBlueprintContext(BlueprintContext blueprintContext) {
		try {
			String macroName = (String) blueprintContext.get("macroName");
			MacroMetadata macroMetadata = macroMetadataManager.getMacroMetadataByName(macroName);
			String pageTitle = Utils.translate("com.seuqra.confluence.macrodoc.macro-information.blueprint.pageName",i18n, Utils.translate(macroMetadata.getTitle().getKey(),i18n));
			String newPageTitle = pageTitle;
			// Check if the page already exists
			if (pageManager.getPage(blueprintContext.getSpaceKey(), pageTitle) != null) {
				// Add (number) after the page name
				int i = 2;
				do {
					newPageTitle = pageTitle+"("+ i++ +")";
				}
				while (pageManager.getPage(blueprintContext.getSpaceKey(), newPageTitle) != null);
			}
			
			blueprintContext.setTitle(newPageTitle);
			blueprintContext.put("macroTitle", Utils.translate(macroMetadata.getTitle().getKey(),i18n));
			blueprintContext.put("macroDescription", Utils.translate(macroMetadata.getDescription().getKey(), i18n));
			// Not safe: But easy way to convert Map<String, Object> to Map<String, String>
			Map<String, String> parameters = (Map) blueprintContext.getMap();
			List <List<String>> parametersList = MacroInformation.createParametersTable(macroMetadata, parameters, i18n);
	    	Map<String, Object> context = MacroUtils.defaultVelocityContext();
			context.put("parametersList", parametersList);
			context.put("onlyTable", "true");
			String result = VelocityUtils.getRenderedTemplate("templates/macro-information.vm", context);
			blueprintContext.put("htmlParametersTable",result);
		} catch (Exception e) {
			log.error("Cannot create Blueprint", e);
		}
		return blueprintContext;
	}
}
