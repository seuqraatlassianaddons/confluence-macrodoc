package com.seuqra.confluence.macrodoc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.browser.MacroMetadataManager;
import com.atlassian.confluence.macro.browser.beans.MacroFormDetails;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.macro.browser.beans.MacroParameter;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.message.I18nResolver;

/**
 **/
public class MacroInformation implements Macro {
	static final String MACRO_INFORMATION = "macro-information";
	private static final String STRING_NAME = "com.seuqra.confluence.macrodoc.macro-information.name";
	private static final String STRING_PARAMETERS = "com.seuqra.confluence.macrodoc.macro-information.parameters";
	private static final String STRING_KEY = "com.seuqra.confluence.macrodoc.macro-information.key";
	private static final String STRING_TYPE = "com.seuqra.confluence.macrodoc.macro-information.type";
	private static final String STRING_DEFAULT_VALUE = "com.seuqra.confluence.macrodoc.macro-information.defaultValue";
	private static final String STRING_DESCRIPTION = "com.seuqra.confluence.macrodoc.macro-information.description";
	private static final String STRING_POSSIBLE_VALUES = "com.seuqra.confluence.macrodoc.macro-information.possibleValues";
	
	private static final String PARAM_DISPLAY_KEY = "displayKey";
	private static final String PARAM_DISPLAY_TYPE = "displayType";
	private static final String PARAM_DISPLAY_DEFAULT_VALUE = "displayDefaultValue";
	private static final String PARAM_DISPLAY_POSSIBLE_VALUES = "displayPossibleValues";

	private static final String ERROR = "com.seuqra.confluence.macrodoc.macro-information.error";

	private static Log log = LogFactory.getLog("com.seuqra.confluence.macrodoc");
	private XhtmlContent xhtmlContent;
	private MacroMetadataManager macroMetadataManager;
	private I18nResolver i18n;
		
    public MacroInformation(XhtmlContent xhtmlContent, MacroMetadataManager macroMetadataManager, I18nResolver i18n){
    	this.xhtmlContent = xhtmlContent;
    	this.macroMetadataManager = macroMetadataManager;
    	this.i18n = i18n;
    }
	
	@Override
	@RequiresFormat(Format.Storage)
	public String execute(Map<String, String> parameters, String xmlBody, ConversionContext conversionContext) throws MacroExecutionException {
    	Map<String, Object> context = MacroUtils.defaultVelocityContext();
		List <MacroDefinition> macroDefinitions = Utils.computeMacroDefinitionList(xmlBody, conversionContext, xhtmlContent);
		for (MacroDefinition macroDefinition : macroDefinitions) {
			MacroMetadata macroMetadata = macroMetadataManager.getMacroMetadataByName(macroDefinition.getName());
			try {
				context.put("title1", Utils.translate(macroMetadata.getTitle().getKey(),i18n));
			} catch (Exception e) {

			}
			try {
				context.put("description", Utils.translate(macroMetadata.getDescription().getKey(), i18n));
			} catch (Exception e) {
			}
			try {
				context.put("title2", Utils.translate(STRING_PARAMETERS,i18n));
			} catch (Exception e) {
			}
			List <List<String>> parametersList = createParametersTable(macroMetadata, parameters, i18n);
			context.put("parametersList", parametersList);
		}
		try {
			String result = VelocityUtils.getRenderedTemplate("templates/macro-information.vm", context);
			return xhtmlContent.convertStorageToView(result, conversionContext);
		} catch (XMLStreamException e) {
			log.error(e);
		} catch (XhtmlException e) {
			log.error(e);		}
		return Utils.translate(ERROR, i18n);
	}
	
	static List<List<String>> createParametersTable(MacroMetadata macroMetadata, Map<String, String> parameters, I18nResolver i18n) {
		MacroFormDetails macroFormDetails = macroMetadata.getFormDetails();
		List<String> macroParameters = new ArrayList<String>();
		List <MacroParameter> macroDetailsParameters = macroFormDetails.getParameters();
		List<List<String>> macrosParameters = new ArrayList<List<String>>();
		int defaultValuePosition = 0;
		int possibleValuesPosition = 0;
		boolean containsDefaultValue = false;
		boolean containsPossibleValues = false;
		macroParameters.add(Utils.translate(STRING_NAME, i18n));
		macroParameters.add(Utils.translate(STRING_DESCRIPTION, i18n));
		boolean displayKey = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_KEY));
		if (displayKey) {
			macroParameters.add(Utils.translate(STRING_KEY, i18n));
		}
		boolean displayType = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_TYPE));
		if (displayType) {
			macroParameters.add(Utils.translate(STRING_TYPE, i18n));
		}
		boolean displayDefaultValue = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_DEFAULT_VALUE));
		if (displayDefaultValue) {
			defaultValuePosition = macroParameters.size();
			macroParameters.add(Utils.translate(STRING_DEFAULT_VALUE, i18n));
		}
		boolean displayPossibleValues = Boolean.parseBoolean(parameters.get(PARAM_DISPLAY_POSSIBLE_VALUES));
		if (displayPossibleValues) {
			possibleValuesPosition = macroParameters.size();
			macroParameters.add(Utils.translate(STRING_POSSIBLE_VALUES, i18n));
		}
		macrosParameters.add(macroParameters);
		for (MacroParameter macroDetailParameter : macroDetailsParameters) {
			macroParameters = new ArrayList<String>();
			macroParameters.add(Utils.translate(macroDetailParameter.getDisplayName().getKey(), i18n));
			macroParameters.add(Utils.translate(macroDetailParameter.getDescription().getKey(), i18n));
			if (displayKey) {
				macroParameters.add(macroDetailParameter.getName());
			}
			if (displayType) {
				macroParameters.add(macroDetailParameter.getType().getName());
			}
			if (displayDefaultValue) {
				String defaultValue = Utils.translate(macroDetailParameter.getDefaultValue(), i18n);
				if (defaultValue != null && defaultValue.length() > 0 ) {
					containsDefaultValue = true;
				}
				macroParameters.add(defaultValue);
			}
			if (displayPossibleValues) {
				if (macroDetailParameter.getEnumValues() != null) {
					macroParameters.add(macroDetailParameter.getEnumValues().toString().replaceAll("\\[|\\]", ""));
					containsPossibleValues = true;
				} else {
					macroParameters.add("");
				}
			}
			macrosParameters.add(macroParameters);
		}
		if ((!containsDefaultValue && displayDefaultValue) || (!containsPossibleValues && displayPossibleValues)) {
			for (List<String> macroP : macrosParameters) {
				// Remove highest index in the list first not to change the position of the other parameter (defaultValue)
				if (!containsPossibleValues && displayPossibleValues) {
					// No default value in any parameter. Remove column
					macroP.remove(possibleValuesPosition);
				}
				if (!containsDefaultValue && displayDefaultValue) {
					// No default value in any parameter. Remove column
					macroP.remove(defaultValuePosition);
				}
			}
		}

		return macrosParameters;		
	}
	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}
	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
