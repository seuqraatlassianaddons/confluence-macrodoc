package com.seuqra.confluence.macrodoc;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;

import com.atlassian.confluence.macro.browser.beans.MacroFormDetails;
import com.atlassian.confluence.macro.browser.beans.MacroIcon;
import com.atlassian.confluence.macro.browser.beans.MacroMetadata;
import com.atlassian.confluence.macro.browser.beans.MacroPropertyPanelButton;
import com.atlassian.confluence.util.i18n.Message;
import com.atlassian.sal.api.message.I18nResolver;

@XmlRootElement(name="macroMetaData")
public class MacroMetaDataResourceModel implements Comparable<Object> {
	@XmlElement
	private final String macroName;
	@XmlElement
	private final String macroNameText;
	@XmlElement
	private final String pluginKey;
	private final MacroIcon icon;
	@XmlElement
	private final String title;
	@XmlElement
	private final String titleText;
	@XmlElement
	private final String description;
	@XmlElement
	private final String descriptionText;
	@XmlElement
	private final Set<String> aliases;
	@XmlElement
	private final Set<String> categories;
	@XmlElement
	private final boolean isBodyDeprecated;
	@XmlElement
	private final boolean hidden;
	private final MacroFormDetails formDetails;
	@XmlElement
	private final String alternateId;
	private final List<MacroPropertyPanelButton> buttons;
	
	public MacroMetaDataResourceModel(MacroMetadata m, I18nResolver i18n) {
		this(m.getMacroName(), m.getPluginKey(), m.getTitle().getKey(), m.getIcon(), m.getDescription().getKey(), m.getAliases(),
				m.getCategories(), m.isBodyDeprecated(),m.isHidden(), m.getFormDetails(), m.getAlternateId(), 
				m.getButtons(), i18n);
	}

	public MacroMetaDataResourceModel(String macroName, String pluginKey, String title, MacroIcon icon, String description, Set<String> aliases,
			Set<String> categories, boolean isBodyDeprecated, boolean hidden, MacroFormDetails formDetails, String alternateId,
			List<MacroPropertyPanelButton> buttons, I18nResolver i18n) {
		this.macroName = macroName;
		this.macroNameText = i18n.getText(macroName);
		this.pluginKey = pluginKey;
		this.icon = icon;
		this.alternateId = alternateId;
		this.aliases = Collections.unmodifiableSet(aliases);
		this.categories = Collections.unmodifiableSet(categories);
		this.isBodyDeprecated = isBodyDeprecated;
		this.hidden = hidden;

		if ((title == null) || (title.equals(macroName)))
			this.title = (pluginKey + "." + macroName + ".label");
		else {
			this.title = title;
		}
		String t = Utils.translate(title, i18n);
		if ("".equals(t)) {
			this.titleText = macroName;
		} else {
			this.titleText = t;
		}

		if (StringUtils.isBlank(description))
			this.description = (pluginKey + "." + macroName + ".desc");
		else {
			this.description = Utils.translate(description, i18n);;
		}
		this.descriptionText = i18n.getText(description);
		this.formDetails = formDetails;
		this.buttons = buttons;
	}

	public String getMacroName() {
		return this.macroName;
	}

	public String getPluginKey() {
		return this.pluginKey;
	}

	public MacroIcon getIcon() {
		return this.icon;
	}

	public Message getTitle() {
		return Message.getInstance(this.title);
	}

	public String getTitleText() {
		return titleText;
	}

	public Message getDescription() {
		return Message.getInstance(this.description);
	}

	public Set<String> getAliases() {
		return this.aliases;
	}

	public Set<String> getCategories() {
		return this.categories;
	}

	public boolean isBodyDeprecated() {
		return this.isBodyDeprecated;
	}

	public boolean isHidden() {
		return this.hidden;
	}

	public String toString() {
		return this.pluginKey + ":" + this.macroName;
	}

	public String getAlternateId() {
		return this.alternateId;
	}

	public MacroFormDetails getFormDetails() {
		return this.formDetails;
	}

	public List<MacroPropertyPanelButton> getButtons() {
		return this.buttons;
	}

	@Override
	public int compareTo(Object arg0) {
		if (arg0 instanceof MacroMetaDataResourceModel) {
			MacroMetaDataResourceModel macroMetaDataResourceModel = (MacroMetaDataResourceModel) arg0;
			return getTitleText().compareToIgnoreCase(macroMetaDataResourceModel.getTitleText());
		} else {
			return 1;
		}
	}
}
