Confluence.Blueprint.setWizard('com.seuqra.confluence.macrodoc:macro-information-blueprint-item', function(wizard) {
    wizard.on('post-render.macroInformation2', function(e, state) {
	    fetchAndDisplayTasks = function() {
	        $tasksContainer = $("#rest-tasks");
	        $tasksContainer.html("Loading...");
	        $.ajax({
	            type: 'GET',
	            url: AJS.contextPath() + '/rest/macro/1.0/macros',
	            contentType:'application/json'
	        }).done(function(tasks) {
	            var $tasksList = $("<ul />");
	            $tasksContainer.html($tasksList);
	            for (var i = 0; i < tasks.length; ++i) {
	                var task = tasks[i];
	                var $taskItem = $("<li/>");
	                var $taskTitleSpan = $("<span/>");
	                $taskTitleSpan.text(task.macroName);
	                $taskItem.append($taskTitleSpan);
	                $tasksList.append($taskItem);
	            }
	            if ($tasksList.find("li").length == 0) {
	                $tasksContainer.html("<p>No tasks have been created yet. Fill out the form below to create a task.</p>");
	            }
	        }).fail(function() {
	            clearMessages();
	            AJS.messages.error("#aui-message-bar", {
	               body: "<p>Failed to fetch tasks.</p>",
	               closeable: true,
	               shadowed: true
	            });
	        });
	    };
	
	    // Fetch tasks on page load
	    fetchAndDisplayTasks();
   });
});

Confluence.Blueprint.setWizard('com.seuqra.confluence.macrodoc:macro-information-blueprint-item', function(wizard) {
    wizard.on('post-render.macroInformation', function(e, state) {
	    fetchAndDisplayMacros = function() {
	        $macrosContainer = $("#macroName");
	        $macrosContainer.append("<option value='Loading'> Loading...</option>");
	        $.ajax({
	            type: 'GET',
	            url: AJS.contextPath() + '/rest/macro/1.0/macros',
	            contentType:'application/json'
	        }).done(function(macros) {
	        	$macrosContainer.empty();
	            for (var i = 0; i < macros.length; ++i) {
	            	var macro = macros[i];
	                $macrosContainer.append("<option value='" + macro.macroName+ "'>" + macro.titleText + "</option>");
	            }
	            if ($macrosContainer.find("option").length == 0) {
	                $macrosContainer.html("<p>No macro have been defined.</p>");
	            }
	        }).fail(function() {
	            clearMessages();
	            AJS.messages.error("#aui-message-bar", {
	               body: "<p>Failed to fetch macros.</p>",
	               closeable: true,
	               shadowed: true
	            });
	        });
	    };
	
	    // Fetch macros on page load
	    fetchAndDisplayMacros();
   });
});