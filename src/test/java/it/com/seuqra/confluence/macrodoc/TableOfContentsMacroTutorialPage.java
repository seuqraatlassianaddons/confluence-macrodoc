package it.com.seuqra.confluence.macrodoc;

public class TableOfContentsMacroTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Table+of+Contents+Tutorial";
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}

}
