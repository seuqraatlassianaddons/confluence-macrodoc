package it.com.seuqra.confluence.macrodoc;

public class BlogPostsMacroTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Blog+Posts+Tutorial";
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}
}
