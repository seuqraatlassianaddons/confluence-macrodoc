package it.com.seuqra.confluence.macrodoc;

public class SectionColumnCodeBlockMacroTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Section+Column+Code+Block+Tutorial";

	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}

}
