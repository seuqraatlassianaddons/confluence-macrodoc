package it.com.seuqra.confluence.macrodoc;

public class TableOfContentZoneTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Table+of+Content+Zone+Tutorial";
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}

}
