package it.com.seuqra.confluence.macrodoc;

import static com.atlassian.pageobjects.elements.query.Poller.*;
import static org.hamcrest.CoreMatchers.*;

import org.hamcrest.Matcher;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceLoginPage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.TestedProductFactory;

public class TestMacroTutorialPage {
	private static ConfluenceTestedProduct confluenceTestedProduct = TestedProductFactory.create(ConfluenceTestedProduct.class);
	
	private void testIfTableExists(Class tutorialPage, String tableId, boolean shouldExist) {
		Page page = confluenceTestedProduct.getPageBinder().navigateToAndBind(tutorialPage);
		MacroTutorialPage macroTutorialPage = (MacroTutorialPage) page;
		//assertEquals("Table macro-parameters not present in page "+page, macroTutorialPage.getMacroParametersTableId().now(), "macro-parameters");
		if (shouldExist) {
			if (MacroTutorialPage.MACRO_PARAMETERS.equals(tableId)) {
				waitUntil(macroTutorialPage.getMacroParametersTableId(), (Matcher<String>) equalTo(tableId));
			} else if (MacroTutorialPage.MACRO_BODY.equals(tableId)) {
				waitUntil(macroTutorialPage.getMacroBodyTableId(), (Matcher<String>) equalTo(tableId));
			}
		} else {
			if (MacroTutorialPage.MACRO_PARAMETERS.equals(tableId)) {
				waitUntil(macroTutorialPage.getMacroParametersTableId(), nullValue());
			} else if (MacroTutorialPage.MACRO_BODY.equals(tableId)) {
				waitUntil(macroTutorialPage.getMacroBodyTableId(), nullValue());
			}
		}
	}
	
	@BeforeClass
	public static void login() {
		// WORKAROUND: Method ConfluenceAbstractPage.doWait() raises always and exception because the condition "return typeof window.ajsScriptsFinishedLoading != 'undefined'" is never true
		// When setupComplete variable is false, this condition is never checked :)
		ConfluenceAbstractPage.setSetupComplete(false);
		ConfluenceLoginPage confluenceLoginPage = confluenceTestedProduct.gotoLoginPage();
		confluenceLoginPage.login("admin", "admin", true);
	}
	
	@AfterClass
	public static void logout() {
		confluenceTestedProduct.logOut();
	}
	
	@Test
	public void testChartTutorialPage() {
		testIfTableExists(ChartMacroTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(ChartMacroTutorialPage.class, MacroTutorialPage.MACRO_BODY, true);
	}
	
	@Test
	public void testCodeBlockTutorialPage() {
		testIfTableExists(CodeBlockMacroTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(CodeBlockMacroTutorialPage.class, MacroTutorialPage.MACRO_BODY, true);
	}
	
	@Test
	public void testSectionTutorialPage() {
		testIfTableExists(SectionColumnCodeBlockMacroTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(SectionColumnCodeBlockMacroTutorialPage.class, MacroTutorialPage.MACRO_BODY, true);
	}

	@Test
	public void testBlogPostsTutorialPage() {
		testIfTableExists(BlogPostsMacroTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(BlogPostsMacroTutorialPage.class, MacroTutorialPage.MACRO_BODY, false);
	}
	
	@Test
	public void testTableOfContentsTutorialPage() {
		testIfTableExists(TableOfContentsMacroTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(TableOfContentsMacroTutorialPage.class, MacroTutorialPage.MACRO_BODY, false);
	}
	
	@Test
	public void testTableOfContentZoneTutorialPage() {
		testIfTableExists(TableOfContentZoneTutorialPage.class, MacroTutorialPage.MACRO_PARAMETERS, true);
		testIfTableExists(TableOfContentZoneTutorialPage.class, MacroTutorialPage.MACRO_BODY, true);
	}
}
