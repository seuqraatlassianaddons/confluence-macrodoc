package it.com.seuqra.confluence.macrodoc;

public class CodeBlockMacroTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Code+Block+Tutorial";
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}

}
