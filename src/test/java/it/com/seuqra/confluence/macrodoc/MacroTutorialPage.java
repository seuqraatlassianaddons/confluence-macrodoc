package it.com.seuqra.confluence.macrodoc;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedQuery;


public abstract class MacroTutorialPage extends ConfluenceAbstractPage {
	static String PREFIX = "/display/TEST/"; 
	static final String MACRO_PARAMETERS = "macro-parameters";
	static final String MACRO_BODY = "macro-body";

	@ElementBy(id = MACRO_PARAMETERS)
	protected PageElement macroParametersTable;

	@ElementBy(id = MACRO_BODY)
	protected PageElement macroBodyTable;

	public TimedQuery<String> getMacroParametersTableId() {
		return macroParametersTable.timed().getAttribute("id");
	}
	public TimedQuery<String> getMacroBodyTableId() {
		return macroBodyTable.timed().getAttribute("id");
	}
}
