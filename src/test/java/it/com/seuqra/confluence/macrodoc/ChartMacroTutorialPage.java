package it.com.seuqra.confluence.macrodoc;

public class ChartMacroTutorialPage extends MacroTutorialPage {
	private static final String PAGE = "Chart+Tutorial";
	
	@Override
	public String getUrl() {
		return PREFIX+PAGE;
	}
}
